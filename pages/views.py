from django.shortcuts import render
import json
import ibm_watson
from ibm_watson import ApiException
from django.http import JsonResponse

def get_reply(request):
    msg = request.GET['usermsg']
    # Invoke a Watson Assistant method

    assistant = ibm_watson.AssistantV2(
        version='2019-02-28',
        iam_apikey='tYqxHrbsGoGTz2XXL7gxdX99i8HG2TH-40LpG50LUFT-',
        url='https://gateway-lon.watsonplatform.net/assistant/api',

    )
    response = assistant.create_session(
        assistant_id='6d0dfac5-fce9-4f65-b651-d061337496d8'
    ).get_result()

    ans = json.dumps(response["session_id"], indent=2)
    print(ans)

    response = assistant.message(
        assistant_id='6d0dfac5-fce9-4f65-b651-d061337496d8',
        session_id=ans.replace('\"', ''),
        input={

            'text': msg,

        }
    ).get_result()

    ans = json.dumps(response["output"]["generic"][0]["text"], indent=2)
    data = {
        'reply': ans
    }
    print(data)

    return JsonResponse(data)




def index(request):
    return render(request,'pages/index.html')

