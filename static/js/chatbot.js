
$('.buttons_slider').hide();

chat_flag = 0

    $('.fa-phone').on('click',function(){
        Swal.fire({
            title: 'Call us Now!',
            text: 'Tel: (+91) 9898977702, 8511131594',
            imageUrl: '/static/img/download1.gif',
            imageWidth: 100,
            imageHeight: 100,
            imageAlt: 'Custom image',
            animation: false,
            footer: '<a href="https://solusofttech.com/contact.php">Check out our contacts page</a>'
        })
    });



    $('.change_slider').on('click',function(){
        $('.buttons_slider').hide();
        var value = $(this).data('value');
        $('#usermsg').val(value);
        document.getElementById("submitmsg").click();
        if(value === 'i3Capture'){
            $('.i3Capture').show();
             $('.i3Capture').slick({
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: false,
             });
        }
        if(value === 'Dashboard'){
            $('.Dashboard').show();
             $('.Dashboard').slick({
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: false,
             });
        }
        if(value === 'n2wdms'){
            $('.n2wdms').show();
            active_slider('.n2wdms');
        }
        if(value === 'iCloudFP'){
            $('.iCloudFP').show();
            active_slider('.iCloudFP');
        }
        else {

        }
    });

function  active_slider(value){
    $(value).slick({
        slidesToShow: 2,
        slidesToScroll: 2,
        infinite: false,
    });
}
$('.form').submit(function(e){
    msg = $('#usermsg').val();
    if($.trim(msg)){
        newmsg = msg.replace(/ /g,'');
        console.log(newmsg);
        if($.trim(newmsg)){
            insertChat("me", msg);
            console.log(msg);
            if (chat_flag === 0){
                user_name = msg;
                 insertChat("solubot", 'Hi ' +user_name +' Please send me your email address, in case we need to reach out further.');
                chat_flag = 1;
            }
            else {

                    if(chat_flag === 1){
                        $('#usermsg').val('');
                        if (/(.+)@(.+){2,}\.(.+){2,}/.test(msg)){
                            chat_flag += 1;
                            insertChat("solubot", 'Thank you visitor for providing your email address');
                            insertChat("solubot","In which product you are interested in? Please select one from the menu below");
                            $('.buttons_slider').show();
                            $('.sub_slider').hide();
                            active_slider('.buttons_slider');
                            return false;
                        }
                        else{
                             insertChat("solubot", 'Hello ' + user_name+ 'Please give me a valid email address.');
                             return false;
                        }
                    }

                    $.ajax({
                        url: "/get_replay",
                        type: 'get',
                        data: {
                            'usermsg': msg,
                            'username':user_name,
                            'chat_flag':chat_flag,
                            // csrfmiddlewaretoken: '{{ csrf_token }}'
                        },
                        dataType: 'json',
                        beforeSend: function () {
                            $('.typing').show();
                            $('.typing_online').hide();

                        },
                        success: function (data2) {
                            if (data2) {

                                console.log(data2.reply);

                                if (data2.reply === 'exit') {
                                    $('.buttons_slider').show();
                                    $('.sub_slider').hide();
                                    insertChat("solubot", 'bye bye.');
                                } else if (data2.reply === null && msg === "back") {
                                    $('.buttons_slider').show();
                                    $('.sub_slider').hide();
                                } else if (data2.reply === null && msg !== "back") {
                                    $('.buttons_slider').show();
                                    $('.sub_slider').hide();
                                    insertChat("solubot", 'I am still a baby bot, but I have forwarded your question to the office');
                                    $('.typing').hide();
                                    $('.typing_online').show();
                                    active_slider('.buttons_slider');
                                }
                                else if(data2.reply === 'sendemail'){
                                    $('.buttons_slider').show();
                                    active_slider('.buttons_slider');
                                    // insertChat("solubot", 'I will get back to you.');
                                }
                                else {
                                    insertChat("solubot", data2.reply);
                                }
                            }
                        },
                        complete: function (data) {
                            $('.typing').hide();
                            $('.typing_online').show();
                        }
                    });
                        chat_flag += 1
            }
        }
        else{
        console.log(newmsg);
        }
    }

    $('#usermsg').val('');
    e.preventDefault();

    // $.post('', $(this).serialize(), function(data){
    //   $('.message').html(data.message);
    //    console.log("in...");
    // });
});

var input = document.getElementById("usermsg");
input.addEventListener("keyup", function(event) {
  if (event.keyCode === 13) {
   event.preventDefault();
   document.getElementById("submitmsg").click();
  }
});


// CHAT BOOT MESSENGER////////////////////////


$(document).ready(function(){
    $(".chat_on").click(function(){
        $(".Layout").toggle();
        $(".chat_on").hide(300);
    });
       $(".chat_close_icon").click(function(){
        $(".Layout").hide();
           $(".chat_on").show(300);
    });
});




    function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
    }

    //-- No use time. It is a javaScript effect.

    console.log(formatAMPM(new Date()));

    function insertChat(who, text, time){
    if (time === undefined){
        time = 0;
    }
    var control = "";
    var date = formatAMPM(new Date());

    if (who == "me"){
        control = '<li>' +
                        '<div class="msj macro">' +
                            // {#'<div class="avatar"><img class="img-circle" style="width:100%;" src="'+ me.avatar +'" /></div>'+#}
                            '<div class="text text-l">' +
                                '<p>'+ text +'</p>' +
                                '<p><small style="color: black;">'+date+'</small></p>' +
                            '</div>' +
                        '</div>' +
                    '</li>';
    }else{
        control = '<li>' +
                        '<div class="msj-rta macro">' +
                            '<div class="text text-r">' +
                                '<p>'+text+'</p>' +
                                '<p><small style="color: black;">'+date+'</small></p>' +
                            '</div>' +
                  '</li>';
    }
    setTimeout(
        function(){
            $('.Messages_list ul').append(control);


            $('body').find('.Messages li').removeClass('scroll');
            $('.Messages li:last').addClass('scroll');
            var $container = $(".Messages");
            var $scrollTo = $('.scroll');
            $container.animate({scrollTop: $scrollTo.offset().top - $container.offset().top +
            $container.scrollTop(), scrollLeft: 0},300);

        }, time);

}
function resetChat(){
    $("ul").empty();
    $('#usermsg').clear;
}
insertChat("solubot", "Hello, my name is SoluBot..");
insertChat("solubot", "I am here to help with your queries but before that, what is your name?");


resetChat();



